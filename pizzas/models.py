from django.db import models


class Pizza(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField(default="Loremp Ipsum")
    prix = models.FloatField(default=0)
    type = models.CharField(default="pizzas",max_length=20)

    def __str__(self):
        return self.name


class ingredient(models.Model):
    pizzas = models.ManyToManyField(Pizza)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Boisson(models.Model):
    name = models.CharField(max_length=200)
    prix = models.FloatField(default=0)
    type = models.CharField(default="boissons",max_length=20)

    def __str__(self):
        return self.name


class Entree(models.Model):
    name = models.CharField(max_length=200)
    prix = models.FloatField(default=0)
    type = models.CharField(default="entrees",max_length=20)


    def __str__(self):
        return self.name


class Menu(models.Model):
    name = models.CharField(max_length=200)
    pizza = models.ForeignKey(Pizza, on_delete=models.CASCADE)
    entree = models.ForeignKey(Entree, on_delete=models.CASCADE)
    boisson = models.ForeignKey(Boisson, on_delete=models.CASCADE)
    prix = models.FloatField(default=0)
    type = models.CharField(default="menus",max_length=20)


    def __str__(self):
        return self.name
