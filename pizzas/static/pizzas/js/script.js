$(document).ready(function () {
    $('#lunchReglement').click(function (e) {
        e.preventDefault();
        $('#reglement').slideDown();
        $('html, body').animate({
            scrollTop: $("#reglement").offset().top
        }, 1200);
        $("#prenom").focus();
    });

    $('#validateCommande').click(function (e) {
        e.preventDefault();
        $('#resume').slideDown();
        $('#reglement').slideUp();
        $('#start').slideUp();
        $("#resumePrenon").append($('#prenom').val() + " " + $('#nomFamille').val())
        $("#resumeAdresse").append($('#adresse').val() + " <br /> " + $('#codePostal').val() + " " + $('#ville').val())
        $('html, body').animate({
            scrollTop: $("#resume").offset().top
        }, 1200);
        $("#prenom").focus();
    })
})