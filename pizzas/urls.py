from django.conf.urls import url

from . import views

app_name = 'pizza'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    #Panier
    url(r'^add$', views.ajoutPanier, name='addPanier'),
    url(r'^remove$', views.removePanier, name='removePanier'),
    url(r'^panier$', views.panier, name='panier'),
    url(r'^boisson$', views.BoissonView.as_view(), name='boisson'),
]
