from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from django.views import generic
from .models import Pizza, ingredient, Menu, Boisson, Entree
from django.utils import timezone
from django.template import RequestContext
from django.core.urlresolvers import reverse


def index(request):
    all_boisson = Boisson.objects.order_by('-name')[:3]
    all_entrees = Entree.objects.order_by('-name')[:3]
    all_menu = Menu.objects.order_by('-name')[:3]
    alls_pizza = Pizza.objects.order_by('-name')[:6]
    return render(request, 'pizzas/index.html', {'all_pizza': alls_pizza, 'all_boisson': all_boisson, 'all_menu': all_menu})


class DetailView(generic.DetailView):
    model = Pizza
    template_name = 'pizzas/detail.html'


def ajoutPanier(request):
    if request.method == 'POST':
        if request.POST['type'] == "pizza":
            if not request.session.get('pizza_id'):
                request.session['pizza_id'] = []
            pizza_id = request.POST['pizza_id']
            pizza_list = request.session['pizza_id']
            pizza_list = pizza_list + [pizza_id]
            request.session['pizza_id'] = pizza_list

        elif request.POST['type'] == "boisson":
            if not request.session.get('boisson_id'):
                request.session['boisson_id'] = []
            boisson_id = request.POST['boisson_id']
            boisson_list = request.session['boisson_id']
            boisson_list = boisson_list + [boisson_id]
            request.session['boisson_id'] = boisson_list

        elif request.POST['type'] == "menu":
            if not request.session.get('menu_id'):
                request.session['menu_id'] = []
            menu_id = request.POST['menu_id']
            menu_list = request.session['menu_id']
            menu_list = menu_list + [menu_id]
            request.session['menu_id'] = menu_list

        elif request.POST['type'] == "entree":
            if not request.session.get('entree_id'):
                request.session['entree_id'] = []
            entree_id = request.POST['entree_id']
            entree_list = request.session['entree_id']
            entree_list = entree_list + [entree_id]
            request.session['entree_id'] = entree_list
        return HttpResponseRedirect(reverse('pizza:panier'))

    else:
        request.session.flush()
        return HttpResponseRedirect(reverse('pizza:index'))


def panier(request):
    total_ttc = 0;

    if not request.session.get('pizza_id'):
        request.session['pizza_id'] = []
    if not request.session.get('boisson_id'):
        request.session['boisson_id'] = []

    pizza_obj = []
    boisson_obj = []
    menu_obj = []

    for pizza_id in request.session['pizza_id']:
        thisPizza = Pizza.objects.get(id=pizza_id)
        total_ttc += thisPizza.prix
        pizza_obj = pizza_obj + [thisPizza]

    for boisson_id in request.session['boisson_id']:
        thisboisson = Boisson.objects.get(id=boisson_id)
        total_ttc += thisboisson.prix
        boisson_obj = boisson_obj + [thisboisson]

    for menu_id in request.session['menu_id']:
        thismenu = Menu.objects.get(id=menu_id)
        total_ttc += thismenu.prix
        menu_obj = menu_obj + [thismenu]

    pizza_obj = pizza_obj + boisson_obj + menu_obj
    return render(request, 'pizzas/panier.html',
                  {'pizza_commander': pizza_obj, 'total_ttc': total_ttc})


def removePanier(request):
    pizza_id = request.POST['pizza_id']
    index_remove = request.session['pizza_id'].index(pizza_id)
    del request.session['pizza_id'][index_remove]
    # return HttpResponse(request.session['pizza_id'])
    return HttpResponseRedirect(reverse('pizza:panier'))


class BoissonView(generic.ListView):
    template_name = 'pizzas/boisson.html'
    context_object_name = 'all_boisson'

    def get_queryset(self):
        """Return the last five published questions."""
        return
