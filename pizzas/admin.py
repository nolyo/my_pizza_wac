from django.contrib import admin
from .models import Pizza,ingredient,Menu, Entree, Boisson


class PizzaAdmin(admin.ModelAdmin):
    fieldsets = [
        ("Nom", {'fields': ['pizza_name']}),
        ('Date publication', {'fields': ['created_at']}),
    ]

admin.site.register(Pizza)
admin.site.register(ingredient)
admin.site.register(Entree)
admin.site.register(Boisson)
admin.site.register(Menu)
